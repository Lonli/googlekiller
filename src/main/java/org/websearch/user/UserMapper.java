package org.websearch.user;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.websearch.registration.RegistrationDto;
import org.websearch.user.dto.UserDto;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDto(User user);

    User toEntity(RegistrationDto dto);
}
