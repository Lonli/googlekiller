package org.websearch.user;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.websearch.config.security.AuthUser;
import org.websearch.user.dto.UserDto;

@RestController
@RequestMapping("/api")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/me")
    public UserDto getUser(@AuthenticationPrincipal AuthUser user) {
        return userService.findUserById(user.getId());
    }
}