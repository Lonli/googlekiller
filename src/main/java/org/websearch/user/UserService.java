package org.websearch.user;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.websearch.config.security.AuthUser;
import org.websearch.exceptions.ConflictException;
import org.websearch.exceptions.ObjectNotFoundException;
import org.websearch.registration.RegistrationDto;
import org.websearch.user.dto.UserDto;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository repository;
    private final PasswordEncoder encoder;

    public UserService(UserRepository repository, PasswordEncoder encoder) {
        this.repository = repository;
        this.encoder = encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        return repository.findUserByEmail(email)
                .map(AuthUser::new)
                .orElseThrow(() -> new UsernameNotFoundException("No users with email " + email + " were found."));
    }

    public void createUser(RegistrationDto reg) {
        try {
            User user = UserMapper.INSTANCE.toEntity(reg);
            user.setPassword(encoder.encode(user.getPassword()));
            repository.save(user);
        } catch (DuplicateKeyException e) {
            throw new ConflictException("A user with the email address " + reg.getEmail() + " already exists.");
        }
    }

    public UserDto findUserById(long id) {
        return repository.findById(id)
                .map(UserMapper.INSTANCE::toDto)
                .orElseThrow(() -> new ObjectNotFoundException("No users with id " + id + " were found."));
    }
}