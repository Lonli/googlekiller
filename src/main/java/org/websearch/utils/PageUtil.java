package org.websearch.utils;

public class PageUtil {

    public static int getNumOfPages(int pageItemNum, int maxNumPerPage) {
        if (pageItemNum == 0) {
            return 1;
        }
        if (pageItemNum < 0) {
            throw new IndexOutOfBoundsException("The total page item number cannot be negative");
        }
        if (maxNumPerPage <= 0) {
            throw new IndexOutOfBoundsException("Max number of items per page cannot be zero or less");
        }
        return (int) Math.ceil( (float) pageItemNum / maxNumPerPage);
    }

    public static int getFirstItemIndex(int page, int maxNumPerPage) {
        if (page <= 0) {
            throw new IndexOutOfBoundsException("Page cannot be zero or less");
        }
        if (maxNumPerPage <= 0) {
            throw new IndexOutOfBoundsException("Max number of items per page cannot be zero or less");
        }
        return (page - 1) * maxNumPerPage;
    }
}