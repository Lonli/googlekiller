package org.websearch.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

    @GetMapping("/index")
    public String index() {
        return "index-page";
    }

    @GetMapping("/search")
    public String search() {
        return "search-page";
    }

    @GetMapping("/search-result")
    public String searchResult() {
        return "search-result-page";
    }

    @GetMapping("/history")
    public String history() {
        return "history-page";
    }
}