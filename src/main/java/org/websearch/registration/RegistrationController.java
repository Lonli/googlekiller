package org.websearch.registration;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.websearch.user.UserService;

@Controller
public class RegistrationController {

    private final UserService userService;
    private final Validator regValidator;

    public RegistrationController(UserService userService, RegValidator regValidator) {
        this.userService = userService;
        this.regValidator = regValidator;
    }

    @GetMapping("/registration")
    public String view(Model model) {
        RegistrationDto form = new RegistrationDto();

        model.addAttribute("registrationForm", form);
        return "registration";
    }

    @PostMapping("/registration")
    public String register(@ModelAttribute("registrationForm") @Validated RegistrationDto reg,
                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.createUser(reg);
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(reg.getEmail(), reg.getPassword());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        return "redirect:/index";
    }

    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }

        if (target.getClass() == RegistrationDto.class) {
            dataBinder.setValidator(regValidator);
        }
    }
}