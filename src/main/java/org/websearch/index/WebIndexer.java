package org.websearch.index;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.websearch.exceptions.FileNotFoundException;

import java.io.IOException;

import static org.apache.lucene.index.IndexWriterConfig.OpenMode.CREATE_OR_APPEND;

@Component
public class WebIndexer {

    private static final Logger logger = LoggerFactory.getLogger(WebIndexer.class);

    private final Directory directory;
    private final StandardAnalyzer analyzer;

    public WebIndexer(Directory directory,
                      StandardAnalyzer analyzer) {
        this.directory = directory;
        this.analyzer = analyzer;
    }

    public synchronized void save(String title, String description, String url) {
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setOpenMode(CREATE_OR_APPEND);
        try (IndexWriter writer = new IndexWriter(directory, config)) {
            Document doc = new Document();
            doc.add(new TextField("title", title, Field.Store.YES));
            doc.add(new StringField("description", description, Field.Store.YES));
            doc.add(new StringField("query", url, Field.Store.YES));
            writer.addDocument(doc);

        } catch (IOException e) {
            logger.error("Lucene directory mismatch.", e);
            throw new FileNotFoundException("Lucene directory mismatch.");
        }
    }
}