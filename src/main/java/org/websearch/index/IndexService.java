package org.websearch.index;

import org.springframework.stereotype.Service;

@Service
public class IndexService {

    private final WebCrawler crawler;

    public IndexService(WebCrawler crawler) {
        this.crawler = crawler;
    }

    public void index(String url) {
        crawler.crawl(url, 0);
    }
}