package org.websearch.index;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

@Component
public class WebCrawler {

    private static final Logger logger = LoggerFactory.getLogger(WebCrawler.class);

    private final int maxScanDepth;
    private final ExecutorService executor;
    private final String connectAgent;
    private final String connectReferrer;
    private final int connectTimeoutMS;
    private final WebParser parser;
    private final Set<String> visitedUrls = new HashSet<>();

    public WebCrawler(@Value("${crawler.search.depth}") int maxScanDepth,
                      @Value("${crawler.search.threads}") int numberOfThreads,
                      @Value("${crawler.search.agent}") String connectAgent,
                      @Value("${crawler.search.referrer}") String connectReferrer,
                      @Value("${crawler.search.timeoutMS}") int connectTimeoutMS,
                      WebParser parser) {
        this.maxScanDepth = maxScanDepth;
        this.executor = Executors.newFixedThreadPool(numberOfThreads);
        this.connectAgent = connectAgent;
        this.connectReferrer = connectReferrer;
        this.connectTimeoutMS = connectTimeoutMS;
        this.parser = parser;
    }

    public void crawl(String url, int currentDepth) {
        try {
            Document doc = connect(url);
            visitedUrls.add(url);
            parser.parse(url, doc);

            Elements links = doc.select("a");
            int linkDepth = currentDepth + 1;
            for (Element link : links) {
                String href = link.attr("href");
                if ((maxScanDepth > linkDepth) && isUrl(href) && !visitedUrls.contains(href)) {
                    executor.execute(() -> crawl(href, linkDepth));
                }
            }
        } catch (IOException e) {
            logger.error(format("Connection to %s failed!", url), e);
        }
    }

    private Document connect(String url) throws IOException {
        return Jsoup.connect(url)
                .userAgent(connectAgent)
                .referrer(connectReferrer)
                .timeout(connectTimeoutMS)
                .get();
    }

    private boolean isUrl(String url) {
        return Pattern.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", url);
    }

    @PreDestroy
    private void preDestroy() {
        executor.shutdown();
    }
}