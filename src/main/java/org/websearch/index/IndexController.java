package org.websearch.index;

import org.springframework.web.bind.annotation.*;
import org.websearch.index.dto.UrlDto;

@RestController
@RequestMapping("/api/index")
public class IndexController {

    private final IndexService indexService;

    public IndexController(IndexService indexService) {
        this.indexService = indexService;
    }

    @PostMapping
    public void index(@RequestBody UrlDto url) {
        indexService.index(url.getUrl());
    }
}