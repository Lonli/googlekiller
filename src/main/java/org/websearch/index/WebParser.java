package org.websearch.index;

import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

@Component
public class WebParser {
    private final int descBoundary;
    private final WebIndexer collector;

    public WebParser(@Value("${collector.description-boundary}") int descBoundary,
                     WebIndexer collector) {
        this.descBoundary = descBoundary;
        this.collector = collector;
    }

    public void parse(String url, Document doc) {
        if (nonNull(doc.title()) && nonNull(doc.body())) {
            String title = doc.title();
            String body = doc.body().text();
            String truncatedBody = body.substring(0, Math.min(body.length(), descBoundary)) + "...";

            collector.save(title, truncatedBody, url);
        }
    }
}
