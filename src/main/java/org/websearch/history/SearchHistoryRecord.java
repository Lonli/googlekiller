package org.websearch.history;

import java.time.Instant;

public class SearchHistoryRecord {

    private long id;
    private String query;
    private Instant date;

    public SearchHistoryRecord() {
    }

    public SearchHistoryRecord(String query) {
        this.query = query;
        this.date = Instant.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
}
