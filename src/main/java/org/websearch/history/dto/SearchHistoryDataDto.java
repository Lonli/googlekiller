package org.websearch.history.dto;

import java.util.List;

public class SearchHistoryDataDto {

    private List<SearchHistoryRecordDto> records;
    private int maxPages;

    public SearchHistoryDataDto(List<SearchHistoryRecordDto> records, int maxPages) {
        this.records = records;
        this.maxPages = maxPages;
    }

    public List<SearchHistoryRecordDto> getRecords() {
        return records;
    }

    public int getMaxPages() {
        return maxPages;
    }
}
