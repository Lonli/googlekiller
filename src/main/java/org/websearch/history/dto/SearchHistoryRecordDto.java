package org.websearch.history.dto;

import java.time.Instant;

public class SearchHistoryRecordDto {

    private long id;
    private String query;
    private Instant date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
}
