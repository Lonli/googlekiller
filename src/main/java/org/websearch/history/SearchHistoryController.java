package org.websearch.history;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.websearch.config.security.AuthUser;
import org.websearch.history.dto.SearchHistoryDataDto;

@RestController
@RequestMapping("/api")
public class SearchHistoryController {

    private final SearchHistoryService searchHistoryService;

    public SearchHistoryController(SearchHistoryService searchHistoryService) {
        this.searchHistoryService = searchHistoryService;
    }

    @GetMapping("/history")
    public SearchHistoryDataDto getByUserId(@AuthenticationPrincipal AuthUser user, @RequestParam("p") int page) {
        return searchHistoryService.getByUserId(user.getId(), page);
    }

    @DeleteMapping("/history")
    public void deleteByUserId(@AuthenticationPrincipal AuthUser user) {
        searchHistoryService.deleteByUserId(user.getId());
    }
}