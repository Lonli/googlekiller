package org.websearch.history;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.websearch.history.dto.SearchHistoryRecordDto;

@Mapper
public interface SearchHistoryMapper {

    SearchHistoryMapper INSTANCE = Mappers.getMapper(SearchHistoryMapper.class);

    SearchHistoryRecordDto toDto(SearchHistoryRecord record);
}
