package org.websearch.history;

import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface SearchHistoryRepository extends CrudRepository<SearchHistoryRecord, Long> {

    @Modifying
    @Query("INSERT INTO history (query, date, user_id) VALUES (:query, :date, :userId);")
    void save(@Param("query") String query,
              @Param("date") Instant date,
              @Param("userId") long userId);

    @Query("SELECT id, query, date, user_id AS userId FROM history WHERE user_id = :userId ORDER BY date DESC " +
            "LIMIT :count OFFSET :start;")
    List<SearchHistoryRecord> getByUserId(@Param("userId") long userId, @Param("start") long start, @Param("count") int count);

    @Query("SELECT COUNT(*) FROM history WHERE user_id = :userId;")
    int countRecordsByUserId(@Param("userId") long userId);

    @Modifying
    @Query("DELETE FROM history WHERE user_id = :userId;")
    void deleteByUserId(@Param("userId") long userId);
}