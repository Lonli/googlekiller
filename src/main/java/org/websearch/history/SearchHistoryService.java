package org.websearch.history;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.websearch.history.dto.SearchHistoryDataDto;
import org.websearch.history.dto.SearchHistoryRecordDto;
import org.websearch.utils.PageUtil;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchHistoryService {

    private final int maxRecordsPerPage;
    private final SearchHistoryRepository repository;

    public SearchHistoryService(@Value("${history.page-size}") int maxRecordsPerPage, SearchHistoryRepository repository) {
        this.maxRecordsPerPage = maxRecordsPerPage;
        this.repository = repository;
    }

    public void save(long userId, SearchHistoryRecord record) {
        repository.save(record.getQuery(), record.getDate(), userId);
    }

    public SearchHistoryDataDto getByUserId(long userId, int page) {
        long startIndex = PageUtil.getFirstItemIndex(page, maxRecordsPerPage);

        List<SearchHistoryRecordDto> records = repository.getByUserId(userId, startIndex, maxRecordsPerPage)
                .stream()
                .map(SearchHistoryMapper.INSTANCE::toDto)
                .collect(Collectors.toList());

        int numOfRecords = repository.countRecordsByUserId(userId);
        int numOfPages = PageUtil.getNumOfPages(numOfRecords, maxRecordsPerPage);

        return new SearchHistoryDataDto(records, numOfPages);
    }

    public void deleteByUserId(long userId) {
        repository.deleteByUserId(userId);
    }
}