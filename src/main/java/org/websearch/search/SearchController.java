package org.websearch.search;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.websearch.config.security.AuthUser;
import org.websearch.search.dto.SearchDataDto;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("/api/search")
public class SearchController {

    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping
    public SearchDataDto search(@AuthenticationPrincipal AuthUser user,
                                @RequestParam("q") String query,
                                @RequestParam(value = "p", defaultValue = "1", required = false) int page) {
        return searchService.search(user, query, page);
    }
}