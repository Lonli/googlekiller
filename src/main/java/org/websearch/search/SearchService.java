package org.websearch.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.websearch.config.security.AuthUser;
import org.websearch.history.SearchHistoryRecord;
import org.websearch.history.SearchHistoryService;
import org.websearch.search.dto.SearchDataDto;

import static java.lang.String.format;
import static java.util.Objects.nonNull;

@Service
public class SearchService {

    private final WebSearcher reader;
    private final SearchHistoryService history;

    private static final Logger logger = LoggerFactory.getLogger(SearchService.class);

    public SearchService(WebSearcher reader, SearchHistoryService history) {
        this.reader = reader;
        this.history = history;
    }

    public SearchDataDto search(AuthUser user, String query, int page) {
        if (nonNull(user)) {
            history.save(user.getId(), new SearchHistoryRecord(query));
        } else {
            logger.info("The user is not authorized. Search history will not be saved.");
        }

        return reader.search(query, page);
    }
}