package org.websearch.search;

import org.apache.lucene.document.Document;
import org.springframework.stereotype.Component;
import org.websearch.search.dto.SearchResultDto;

@Component
public class SearchConverter {

    public SearchResultDto convert(Document document) {
        String title = document.getField("title").stringValue();
        String description = document.getField("description").stringValue();
        String query = document.getField("query").stringValue();

        return new SearchResultDto(title, description, query);
    }
}
