package org.websearch.search;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.websearch.exceptions.FileNotFoundException;
import org.websearch.search.dto.SearchDataDto;
import org.websearch.search.dto.SearchResultDto;
import org.websearch.utils.PageUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

@Component
public class WebSearcher {

    private static final Logger logger = LoggerFactory.getLogger(WebSearcher.class);

    private final int maxResultsPerPage;
    private final SearchConverter converter;
    private final Directory directory;
    private final StandardAnalyzer analyzer;

    public WebSearcher(@Value("${collector.page-size}") int maxResultsPerPage,
                       SearchConverter converter,
                       Directory directory,
                       StandardAnalyzer analyzer) {
        this.maxResultsPerPage = maxResultsPerPage;
        this.converter = converter;
        this.directory = directory;
        this.analyzer = analyzer;
    }

    public SearchDataDto search(String keywords, int page) {
        Query query = getQuery(keywords);
        TopFieldCollector collector = TopFieldCollector.create(Sort.RELEVANCE, 1000, 0);

        try (IndexReader indexReader = DirectoryReader.open(directory)) {
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            indexSearcher.search(query, collector);
            ScoreDoc[] scoreDocs = getDocsForPage(page, collector);

            List<SearchResultDto> results = getSearchResults(indexSearcher, scoreDocs);
            int pages = PageUtil.getNumOfPages(collector.getTotalHits(), maxResultsPerPage);

            return new SearchDataDto(results, pages);
        } catch (IOException e) {
            logger.error("Lucene directory mismatch.", e);
            throw new FileNotFoundException("Lucene directory mismatch.");
        }
    }

    private Query getQuery(String keywords) {
        try {
            QueryParser parser = new QueryParser("title", analyzer);

            return parser.parse(keywords);
        } catch (ParseException e) {
            logger.error(format("Cannot get query from '%s'", keywords), e);
            throw new IllegalArgumentException("Cannot handle this request!");
        }
    }

    private ScoreDoc[] getDocsForPage(int page, TopFieldCollector collector) {
        int startIndex = PageUtil.getFirstItemIndex(page, maxResultsPerPage);
        TopDocs docs = collector.topDocs(startIndex, maxResultsPerPage);
        return docs.scoreDocs;
    }

    private List<SearchResultDto> getSearchResults(IndexSearcher searcher, ScoreDoc[] scoreDocs) throws IOException {
        List<SearchResultDto> results = new ArrayList<>();

        for (ScoreDoc scoreDoc : scoreDocs) {
            Document document = searcher.doc(scoreDoc.doc);
            results.add(converter.convert(document));
        }
        return results;
    }
}