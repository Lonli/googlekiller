package org.websearch.search.dto;

import java.util.List;

public class SearchDataDto {

    private List<SearchResultDto> results;
    private int maxPages;

    public SearchDataDto() {
    }

    public SearchDataDto(List<SearchResultDto> results, int maxPages) {
        this.results = results;
        this.maxPages = maxPages;
    }

    public List<SearchResultDto> getResults() {
        return results;
    }

    public int getMaxPages() {
        return maxPages;
    }
}
