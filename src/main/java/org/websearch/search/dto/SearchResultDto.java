package org.websearch.search.dto;

public class SearchResultDto {

    private String title;
    private String description;
    private String query;

    public SearchResultDto() {
    }

    public SearchResultDto(String title, String description, String query) {
        this.title = title;
        this.description = description;
        this.query = query;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
