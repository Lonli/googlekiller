package org.websearch.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.websearch.exceptions.dto.ErrorDto;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class RestExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(FileNotFoundException.class)
    public ErrorDto handle(FileNotFoundException e) {
        String message = e.getMessage();
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ResponseStatus(CONFLICT)
    @ExceptionHandler(ConflictException.class)
    public ErrorDto handle(ConflictException e) {
        String message = e.getMessage();
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(ObjectNotFoundException.class)
    public ErrorDto handle(ObjectNotFoundException e) {
        String message = e.getMessage();
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(UsernameNotFoundException.class)
    public ErrorDto handle(UsernameNotFoundException e) {
        String message = e.getMessage();
        logger.error(message, e);
        return new ErrorDto(message);
    }
}
