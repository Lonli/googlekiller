package org.websearch.exceptions;

public class ConflictException extends RuntimeException {

    public ConflictException(String s) {
        super(s);
    }

}