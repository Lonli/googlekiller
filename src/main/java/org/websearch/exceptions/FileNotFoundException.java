package org.websearch.exceptions;

public class FileNotFoundException extends RuntimeException {

    public FileNotFoundException(String s) {
        super(s);
    }

}
