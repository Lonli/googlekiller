create table users
(
    id         serial unique,
    first_name varchar(64)                not null,
    last_name  varchar(64)                not null,
    email      varchar(64) unique         not null,
    password   varchar(64)                not null,
    role       varchar(16) default 'USER',

    primary key (id)
);