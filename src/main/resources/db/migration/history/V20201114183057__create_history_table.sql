CREATE TABLE history
(
    id            serial unique,
    query         varchar(256) default NULL,
    date          timestamp    not null,
    user_id       int8         not null,
    primary key (id)
);

alter table if exists history
    add foreign key (user_id) references users;
