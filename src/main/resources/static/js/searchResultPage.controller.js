$(document).ready(function() {
    const params = new URLSearchParams(window.location.search)
    let query = params.get('q');
    let page = params.get('p');

    $(".query-input").val(query);
    let searchUrl = '/api/search?q=' + query + '&p=' + page;

    $.ajax({
        type: "get",
        contentType: 'application/json; charset=utf-8',
        url: searchUrl,
        success: function (data) {
            if (data.results.length > 0) {
                $('.search-result-body').empty()
                $.each(data.results, function (index, value) {
                    let searchItem = getSearchItem(value)
                    $('.search-result-body').append(searchItem);
                });
                for (let i = 1; i <= data.maxPages; i++) {
                    let page = pageButtons(i)
                    $('.search-result-body').append(page);
                }
            } else {
                let emptyContent = noSearchResults(query);
                $('.search-result-body').append(emptyContent);
            }
        },
        error: function (data) {
            $('.search-result-body').empty().append('An empty directory. Please add content first.');
        }
    });
});

function getSearchItem(props) {
    const {title, description, query} = props;

    return `<div class="search-item">
               <p class="query" >${query}</p>
               <a href="${query}">${title}</a>
               <p class="description" >${description}</p>
           </div>`
}

function noSearchResults(query) {
    return `<h2>Oops, no results for "${query}" :(</h2>
            <h2>Please specify your query</h2>`
}

function pageButtons(page) {
    return `<button class="btn-page" onclick="goToPage(${page})">${page}</button>`
}

function goToPage(page) {
    const params = new URLSearchParams(window.location.search)
    let query = params.get('q');
    document.location = '/search-result?q='+query+'&p='+page
}