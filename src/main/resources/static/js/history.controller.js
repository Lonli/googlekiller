$(document).ready(function() {

    const params = new URLSearchParams(window.location.search)
    let page = params.get('p');

    if (!page) {
        page = 1;
    }

    let searchUrl = '/api/history?p=' + page;

    $.ajax({
        type: "get",
        contentType: 'application/json; charset=utf-8',
        url: searchUrl,
        success: function (data) {
            $.each(data.records, function (index, value) {
                let historyItem = getHistoryItem(value)
                $('.history-body').append(historyItem);
            });
            for (let i = 1; i <= data.maxPages; i++) {
                let page = pageButtons(i)
                $('.pages').append(page);
            };
        }
    });
});

function getHistoryItem(props) {
    const {query, date} = props;

    let t = new Date(date)

    let time = t.getDate()+"."+(t.getMonth()+1)+"."+t.getFullYear()+", "+
        ("0" + t.getHours()).slice(-2)+":"+("0" + t.getMinutes()).slice(-2);

    return `<div class="history-item">
               <p>${time}</p>
               <a href="/search-result?q=${query}">${query}</a>
           </div>`
}

$(document).on("click", ".btn-clear-history", function () {
    $.ajax({
        type: "delete",
        contentType: 'application/json; charset=utf-8',
        url: '/api/history',
        success: function () {
            location.reload();
        }
    });
});

function pageButtons(page) {
    return `<button class="btn-page" onclick="goToPage(${page})">${page}</button>`
}

function goToPage(page) {
    document.location = '/history?p='+page
}