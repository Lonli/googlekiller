$(document).on("click", ".btn-save-url", function () {
    let url = $(".url-input").val();

    let urlJson = {
        "url": url
    };

    $.ajax({
        type: "post",
        contentType: 'application/json; charset=utf-8',
        dataType:'text',
        url: '/api/index',
        data: JSON.stringify(urlJson),
        success: function () {
            $('.index-message').empty().append(`<div>Indexing page: ${url}</div>`);
        }
    });
});