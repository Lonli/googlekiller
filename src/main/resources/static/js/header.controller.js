$(document).ready(function() {
    $.ajax({
        type: "get",
        contentType: 'application/json; charset=utf-8',
        url: "/api/users/me",
        success: function (data) {
            if (typeof data === 'object' && data !== null) {
                const {firstName, lastName} = data;
                $('.left').append('<a href="/history">My search history</a> ');
                $('.right').append(`<p>${firstName} ${lastName}</p> `);
                $('.right').append('<a href="/logout">logout</a>');
            } else {
                $('.right').append('<a href="/registration">Registration</a> ');
                $('.right').append('<a href="/login">Login</a>');
            }
        },
        error: function (data) {
            $('.right').append('<a href="/registration">Registration</a> ');
            $('.right').append('<a href="/login">Login</a>');
        }
    });
});