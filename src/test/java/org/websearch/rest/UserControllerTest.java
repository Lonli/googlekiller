package org.websearch.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.websearch.config.security.AuthUser;
import org.websearch.user.*;
import org.websearch.user.dto.UserDto;

import javax.activation.DataSource;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.websearch.user.Role.USER;

@ActiveProfiles("test")
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class)})
@WebMvcTest(UserController.class)
public class UserControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private UserService userService;

    @Autowired
    private WebApplicationContext context;

    private static final long USER_ID = 1;

    private MockMvc mockMvc;
    private AuthUser user;

    @BeforeMethod
    private void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = mock(AuthUser.class);
        when(user.getId()).thenReturn((long) 1);
    }

    @Test
    public void When_UserTryToGetHisInfo_Expect_StatusIsOk() throws Exception {
        UserDto userDto = getUserDto(USER_ID, "first", "last", "test@mail.com", "password", USER);
        when(userService.findUserById(anyLong())).thenReturn(userDto);

        mockMvc.perform(get("/api/users/me")
                .with(user(user)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.email").value(userDto.getEmail()))
                .andExpect(jsonPath("$.firstName").value(userDto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(userDto.getLastName()));
    }

    @Test
    public void When_UnauthorizedUserTryToGetHisInfo_Expect_StatusIsForbidden() throws Exception {
        mockMvc.perform(get("/api/users/me"))
                .andExpect(status().isForbidden());
    }

    private UserDto getUserDto(long id, String firstName, String lastName, String email, String password, Role role) {
        User user = new User();
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        user.setRole(role);

        return UserMapper.INSTANCE.toDto(user);
    }
}
