package org.websearch.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.websearch.config.security.AuthUser;
import org.websearch.history.SearchHistoryController;
import org.websearch.history.SearchHistoryMapper;
import org.websearch.history.SearchHistoryRecord;
import org.websearch.history.SearchHistoryService;
import org.websearch.history.dto.SearchHistoryDataDto;
import org.websearch.history.dto.SearchHistoryRecordDto;
import org.websearch.user.UserService;

import javax.activation.DataSource;
import java.time.Instant;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class), @MockBean(UserService.class)})
@WebMvcTest(SearchHistoryController.class)
public class SearchHistoryControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private SearchHistoryService searchHistoryService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;
    private AuthUser user;

    @BeforeMethod
    private void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = mock(AuthUser.class);
        when(user.getId()).thenReturn((long) 1);
    }

    @Test
    public void When_UserTryToGetSearchHistory_Expect_StatusIsOk() throws Exception {
        SearchHistoryDataDto historyData = getHistoryData();
        when(searchHistoryService.getByUserId(anyLong(), anyInt())).thenReturn(historyData);

        mockMvc.perform(get("/api/history")
                .with(user(user))
                .param("p", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.records").exists())
                .andExpect(jsonPath("$.records").isArray())
                .andExpect(jsonPath("$.records.[0]").exists())
                .andExpect(jsonPath("$.records.[1]").exists())
                .andExpect(jsonPath("$.records.[2]").doesNotExist())
                .andExpect(jsonPath("$.records.[0].id").value(historyData.getRecords().get(0).getId()))
                .andExpect(jsonPath("$.records.[0].query").value(historyData.getRecords().get(0).getQuery()))
                .andExpect(jsonPath("$.records.[0].date").value(historyData.getRecords().get(0).getDate().toString()))
                .andExpect(jsonPath("$.maxPages").value(historyData.getMaxPages()));
    }

    @Test
    public void When_AnonymousUserTryToGetSearchHistory_Expect_StatusIsForbidden() throws Exception {
        SearchHistoryDataDto historyData = getHistoryData();
        when(searchHistoryService.getByUserId(anyLong(), anyInt())).thenReturn(historyData);

        mockMvc.perform(get("/api/history")
                .param("p", "1"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void When_UserTryToDeleteSearchHistory_Expect_StatusIsOk() throws Exception {
        mockMvc.perform(delete("/api/history")
                .with(user(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void When_AnonymousUserTryToDeleteSearchHistory_Expect_StatusIsForbidden() throws Exception {
        mockMvc.perform(delete("/api/history"))
                .andExpect(status().isForbidden());
    }

    private SearchHistoryDataDto getHistoryData() {
        List<SearchHistoryRecordDto> records = List.of(getRecord(1, "First test record"), getRecord(2, "Second test record"));
        return new SearchHistoryDataDto(records, 1);
    }

    private SearchHistoryRecordDto getRecord(long id, String message) {
        SearchHistoryRecord record = new SearchHistoryRecord();
        record.setId(id);
        record.setQuery(message);
        record.setDate(Instant.now());
        return SearchHistoryMapper.INSTANCE.toDto(record);
    }
}
