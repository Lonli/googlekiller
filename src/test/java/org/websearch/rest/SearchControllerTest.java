package org.websearch.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.websearch.config.security.AuthUser;
import org.websearch.search.SearchController;
import org.websearch.search.SearchService;
import org.websearch.search.dto.SearchDataDto;
import org.websearch.search.dto.SearchResultDto;
import org.websearch.user.UserController;
import org.websearch.user.UserService;
import org.websearch.user.dto.UserDto;

import javax.activation.DataSource;

import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.websearch.user.Role.USER;

@ActiveProfiles("test")
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class), @MockBean(UserService.class)})
@WebMvcTest(SearchController.class)
public class SearchControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private SearchService searchService;

    @Autowired
    private WebApplicationContext context;

    private static final long USER_ID = 1;

    private MockMvc mockMvc;

    AuthUser user;

    @BeforeMethod
    private void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        user = mock(AuthUser.class);
        when(user.getId()).thenReturn((long) 1);
    }

    @Test
    public void When_UserTryToGetHisInfo_Expect_StatusIsOk() throws Exception {
        SearchDataDto data = getSearchData();
        when(searchService.search(any(), anyString(), anyInt())).thenReturn(data);

        mockMvc.perform(get("/api/search")
                .with(user(user))
                .param("q", "query")
                .param("p", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.results").exists())
                .andExpect(jsonPath("$.results").isArray())
                .andExpect(jsonPath("$.results.[0]").exists())
                .andExpect(jsonPath("$.results.[1]").exists())
                .andExpect(jsonPath("$.results.[2]").doesNotExist())
                .andExpect(jsonPath("$.results.[0].query").value(data.getResults().get(0).getQuery()))
                .andExpect(jsonPath("$.results.[0].title").value(data.getResults().get(0).getTitle()))
                .andExpect(jsonPath("$.results.[0].description").value(data.getResults().get(0).getDescription()))
                .andExpect(jsonPath("$.maxPages").value(data.getMaxPages()));
    }

    private SearchDataDto getSearchData() {
        List<SearchResultDto> results = List.of(getSearchResult(), getSearchResult());
        int pageNum = 1;

        return new SearchDataDto(results, pageNum);
    }

    private SearchResultDto getSearchResult() {
        SearchResultDto result = new SearchResultDto();
        result.setQuery("Query");
        result.setTitle("Title");
        result.setDescription("Description");
        return result;
    }

}
