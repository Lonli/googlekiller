package org.websearch.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.websearch.registration.RegValidator;
import org.websearch.registration.RegistrationController;
import org.websearch.registration.RegistrationDto;
import org.websearch.user.UserRepository;
import org.websearch.user.UserService;

import javax.activation.DataSource;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@Import(RegValidator.class)
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class), @MockBean(UserService.class), @MockBean(UserRepository.class)})
@WebMvcTest(RegistrationController.class)
public class RegistrationControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void When_UnauthorizedUserGetRegistrationPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_DoPostToRegistration_Expect_Redirect() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .flashAttr("registrationForm", getRegistration()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/index"));
    }

    @Test
    public void When_DoPostWithNotValidRegistration_Expect_IsFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .flashAttr("registrationForm", getNotValidRegistration()))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrors("registrationForm", "firstName"))
                .andExpect(model().attributeHasFieldErrors("registrationForm", "firstName"))
                .andExpect(model().attributeHasFieldErrors("registrationForm", "email"))
                .andExpect(model().attributeHasFieldErrors("registrationForm", "password"))
                .andExpect(model().attributeHasFieldErrors("registrationForm", "confirmPassword"));
    }

    private RegistrationDto getRegistration() {
        RegistrationDto reg = new RegistrationDto();
        reg.setFirstName("first");
        reg.setLastName("last");
        reg.setEmail("email@gmail.com");
        reg.setPassword("password");
        reg.setConfirmPassword("password");
        return reg;
    }

    private RegistrationDto getNotValidRegistration() {
        RegistrationDto reg = new RegistrationDto();
        reg.setFirstName("");
        reg.setLastName("");
        reg.setEmail("");
        reg.setPassword("");
        reg.setConfirmPassword("!");
        return reg;
    }
}
