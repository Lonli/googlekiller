package org.websearch.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.websearch.config.security.AuthUser;
import org.websearch.login.LoginController;
import org.websearch.login.LoginDto;
import org.websearch.login.LoginValidator;
import org.websearch.registration.RegistrationDto;
import org.websearch.user.User;
import org.websearch.user.UserRepository;
import org.websearch.user.UserService;

import javax.activation.DataSource;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@Import(LoginValidator.class)
@MockBeans({@MockBean(DataSource.class), @MockBean(UserService.class)})
@WebMvcTest(LoginController.class)
public class LoginControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    @MockBean
    private PasswordEncoder passwordEncoder;
    @Autowired
    @MockBean
    private UserRepository userRepository;

    private User user;

    @BeforeMethod
    private void setUp() {
        this.user = mock(User.class);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user));
    }

    @Test
    public void When_UnauthorizedUserGetRegistrationPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_DoPostToRegistration_Expect_Redirect() throws Exception {
        when(user.getPassword()).thenReturn("password");
        mockMvc.perform(MockMvcRequestBuilders.post("/api/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .flashAttr("loginForm", getLogin()))
                .andExpect(status().isFound())
                .andExpect(model().hasNoErrors())
                .andExpect(redirectedUrl("/index"));
    }

    @Test
    public void When_DoPostWithNotValidRegistration_Expect_IsFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .flashAttr("loginForm", getNotValidLogin()))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrors("loginForm", "email"))
                .andExpect(model().attributeHasFieldErrors("loginForm", "password"));
    }

    private LoginDto getLogin() {
        LoginDto login = new LoginDto();
        login.setEmail("email@gmail.com");
        login.setPassword("password");
        return login;
    }

    private LoginDto getNotValidLogin() {
        LoginDto login = new LoginDto();
        login.setEmail("");
        login.setPassword("");
        return login;
    }
}
