package org.websearch.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;
import org.websearch.user.UserService;
import org.websearch.view.ViewController;

import javax.activation.DataSource;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class), @MockBean(UserService.class)})
@WebMvcTest(ViewController.class)
public class ViewControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void When_UnauthorizedUserGetIndexPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/index"))
                .andExpect(status().isOk())
                .andExpect(view().name("index-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_AuthorizedUserGetIndexPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/index")
                .with(user("user").password("user").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("index-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_UnauthorizedUserGetSearchPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/search"))
                .andExpect(status().isOk())
                .andExpect(view().name("search-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_AuthorizedUserGetSearchPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/search")
                .with(user("user").password("user").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("search-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_UnauthorizedUserGetSearchResultPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/search-result"))
                .andExpect(status().isOk())
                .andExpect(view().name("search-result-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_AuthorizedUserGetSearchResultPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/search-result")
                .with(user("user").password("user").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("search-result-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }

    @Test
    public void When_UnauthorizedUserGetHistoryPage_Expect_StatusForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/history"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void When_AuthorizedUserGetHistoryPage_Expect_StatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/history")
                .with(user("user").password("user").roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("history-page"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML));
    }
}
