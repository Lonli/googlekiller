package org.websearch;

import org.apache.lucene.index.IndexWriter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@ActiveProfiles("test")
@MockBean(IndexWriter.class)
@SpringBootTest
public class ContextLoaderTest extends AbstractTestNGSpringContextTests {

    @Test
    public void context_started() {
    }

    @Test
    public void application_started() {
        ApiStarter.main(new String[] {});
    }
}
