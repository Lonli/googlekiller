package org.websearch.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import org.websearch.config.security.AuthUser;
import org.websearch.exceptions.ObjectNotFoundException;
import org.websearch.history.SearchHistoryRecord;
import org.websearch.history.SearchHistoryRepository;
import org.websearch.history.SearchHistoryService;
import org.websearch.history.dto.SearchHistoryDataDto;
import org.websearch.registration.RegistrationDto;
import org.websearch.user.*;
import org.websearch.user.dto.UserDto;

import javax.activation.DataSource;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;
import static org.websearch.user.Role.USER;
import java.util.Optional;

@ActiveProfiles("test")
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class)})
@WebMvcTest(UserService.class)
public class UserServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    private static final long USER_ID = 1;
    private static final String USER_FIRST_NAME = "firstName";
    private static final String USER_LAST_NAME = "lastName";
    private static final String USER_EMAIL = "test@mail.com";
    private static final String USER_PASSWORD = "password";

    @Test
    public void When_LoadUserByUsername_Expect_UserDetails() {
        User user = getUser();
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user));

        AuthUser authUser = (AuthUser) userService.loadUserByUsername(USER_EMAIL);

        assertNotNull(authUser);
        assertEquals(authUser.getId(), user.getId());
        assertEquals(authUser.getUsername(), user.getEmail());
        assertEquals(authUser.getPassword(), user.getPassword());
    }

    @Test(expectedExceptions = UsernameNotFoundException.class)
    public void When_LoadNullUserByUsername_Expect_UsernameNotFoundException() {
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.empty());

        userService.loadUserByUsername(USER_EMAIL);
    }

    @Test
    public void When_CreateUser_Expect_Nothing() {
        RegistrationDto reg = getRegistration();

        userService.createUser(reg);
    }

    @Test
    public void When_FindUserById_Expect_UserDto() {
        User user = getUser();
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        UserDto userDto = userService.findUserById(USER_ID);

        assertNotNull(userDto);
        assertEquals(userDto.getEmail(), user.getEmail());
        assertEquals(userDto.getFirstName(), user.getFirstName());
        assertEquals(userDto.getLastName(), user.getLastName());
    }

    @Test(expectedExceptions = ObjectNotFoundException.class)
    public void When_FindNullUserById_Expect_() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        userService.findUserById(USER_ID);
    }

    private User getUser() {
        User user = new User();
        user.setId(USER_ID);
        user.setFirstName(USER_FIRST_NAME);
        user.setLastName(USER_LAST_NAME);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PASSWORD);
        user.setRole(USER);
        return user;
    }

    private RegistrationDto getRegistration() {
        RegistrationDto reg = new RegistrationDto();
        reg.setFirstName(USER_FIRST_NAME);
        reg.setLastName(USER_LAST_NAME);
        reg.setEmail(USER_EMAIL);
        reg.setPassword(USER_PASSWORD);
        reg.setConfirmPassword(USER_PASSWORD);
        return reg;
    }
}