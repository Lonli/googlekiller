package org.websearch.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;
import org.websearch.history.SearchHistoryRecord;
import org.websearch.history.SearchHistoryRepository;
import org.websearch.history.SearchHistoryService;
import org.websearch.history.dto.SearchHistoryDataDto;
import org.websearch.user.UserService;

import javax.activation.DataSource;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

@ActiveProfiles("test")
@MockBeans({@MockBean(DataSource.class), @MockBean(PasswordEncoder.class), @MockBean(UserService.class)})
@WebMvcTest(SearchHistoryService.class)
public class SearchHistoryServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private SearchHistoryRepository searchHistoryRepository;
    @Autowired
    private SearchHistoryService searchHistoryService;

    private static final int FIRST_PAGE = 1;
    private static final int MAX_PAGES = 1;
    private static final int HISTORY_RECORDS_COUNT = 2;
    private static final long USER_ID = 1;

    @Test
    public void When_UserTryToSaveSearchHistory_Expect_Nothing() {
        SearchHistoryRecord record = new SearchHistoryRecord("First test record");
        when(searchHistoryRepository.save(record)).thenReturn(record);

        searchHistoryService.save(USER_ID, record);
    }

    @Test
    public void When_UserTryToGetSearchHistory_Expect_SearchHistoryData() {
        List<SearchHistoryRecord> records = List.of(new SearchHistoryRecord("First test record"), new SearchHistoryRecord("Second test record"));
        when(searchHistoryRepository.getByUserId(anyLong(), anyLong(), anyInt())).thenReturn(records);
        when(searchHistoryRepository.countRecordsByUserId(anyInt())).thenReturn(HISTORY_RECORDS_COUNT);

        SearchHistoryDataDto historyData = searchHistoryService.getByUserId(USER_ID, FIRST_PAGE);

        assertNotNull(historyData);
        assertNotNull(historyData.getRecords());
        assertFalse(historyData.getRecords().isEmpty());
        assertEquals(historyData.getRecords().size(), records.size());
        assertEquals(historyData.getMaxPages(), MAX_PAGES);
    }

    @Test
    public void When_UserTryToDeleteHisSearchHistory_Expect_Nothing() {
        searchHistoryService.deleteByUserId(USER_ID);
    }
}