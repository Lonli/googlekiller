package org.websearch.utils;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PageUtilTest {

    @Test
    public void When_GetItemIndexForPage_Expect_Zero() {
        int index = PageUtil.getFirstItemIndex(1, 5);

        assertEquals(index, 0);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void When_GetItemIndexForIncorrectPage_Expect_IndexOutOfBoundsException() {
        PageUtil.getFirstItemIndex(0, 5);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void When_GetItemIndexForIncorrectMaxNumber_Expect_IndexOutOfBoundsException() {
        PageUtil.getFirstItemIndex(1, 0);
    }

    @Test
    public void When_GetNumOfPages_Expect_One() {
        int numOfPages = PageUtil.getNumOfPages(1, 5);

        assertEquals(numOfPages, 1);
    }

    @Test
    public void When_GetNumOfPagesForZeroItems_Expect_One() {
        int numOfPages = PageUtil.getNumOfPages(0, 5);

        assertEquals(numOfPages, 1);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void When_GetNumOfPagesForNegativeItemsNumber_Expect_IndexOutOfBoundsException() {
        PageUtil.getNumOfPages(-1, 1);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void When_GetNumOfPagesForIncorrectMaxNumber_Expect_IndexOutOfBoundsException() {
        PageUtil.getNumOfPages(1, 0);
    }
}
